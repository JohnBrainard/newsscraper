package com.brainardphotography.scraper.feeds

import com.brainardphotography.scraper.feeds.data.Feed
import com.brainardphotography.scraper.feeds.service.FeedService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import spock.lang.Specification

class FeedApiControllerTest extends Specification {

	private FeedService mockFeedService

	private FeedApiController controller

	void setup() {
		mockFeedService = Mock(FeedService)

		controller = new FeedApiController(mockFeedService)
	}

	def "list() returns news feeds"() {
		given:
		Feed testFeed = new Feed(UUID.randomUUID(), "Android Central", "Android Central", "http://www.androidcentral.com/rss.xml")

		when:
		ResponseEntity<FeedsResponse> response = controller.feeds()

		then:
		1 * mockFeedService.listFeeds() >> [testFeed]

		response.body == new FeedsResponse([
				new FeedResponse(testFeed.id, testFeed.name, testFeed.description, testFeed.url)
		])
	}

	def "createFeed() creates a new feed"() {
		given:
		FeedRequest feedRequest = createFeedRequest()

		when:
		ResponseEntity<FeedResponse> response = controller.createFeed(feedRequest)

		then:
		1 * mockFeedService.getFeedForUrl(feedRequest.url) >> null
		1 * mockFeedService.save(_ as Feed) >> { Feed feed ->
			assert feed.url == feedRequest.url
			assert feed.description == feedRequest.description
			assert feed.url == feedRequest.url

			return new Feed(UUID.randomUUID(), feedRequest.name, feedRequest.description, feedRequest.url)
		}

		response.body.name == feedRequest.name
		response.body.description == feedRequest.description
		response.body.url == feedRequest.url
		response.body.id != null
	}

	def "createFeed() returns CONFLICT (409) if feed exists"() {
		given:
		FeedRequest feedRequest = createFeedRequest()

		when:
		ResponseEntity<FeedRequest> response = controller.createFeed(feedRequest)

		then:
		1 * mockFeedService.getFeedForUrl(feedRequest.url) >> createFeed()
		0 * mockFeedService.save(_ as Feed)

		response.statusCode == HttpStatus.CONFLICT
	}

	def "updateFeed() updates name and description on a new feed"() {
		given:
		FeedRequest request = createFeedRequest("Android Central Updated", "Android Central Updated", "DON'T SET")
		UUID feedUuid = UUID.randomUUID()
		Feed feed = createFeed(feedUuid)

		when:
		ResponseEntity<FeedResponse> response = controller.updateFeed(feedUuid, request)

		then:
		1 * mockFeedService.getFeed(feedUuid) >> feed
		1 * mockFeedService.save(_ as Feed) >> { Feed toSave ->
			assert toSave.id == feedUuid
			assert toSave.name == request.name
			assert toSave.description == request.description
			return feed
		}

		response.body.name == request.name
		response.body.description == request.description
		response.body.url == feed.url
		response.body.id == feedUuid
	}

	def "updateFeed() returns NOT_FOUND (404) for unsaved feeds"() {
		given:
		FeedRequest request = createFeedRequest()

		when:
		ResponseEntity<FeedResponse> response = controller.updateFeed(UUID.randomUUID(), request)

		then:
		1 * mockFeedService.getFeed(_ as UUID) >> null
		0 * mockFeedService.save(_ as Feed)

		response.statusCode == HttpStatus.NOT_FOUND
	}

	private Feed createFeed(UUID uuid = UUID.randomUUID()) {
		return new Feed(uuid, "Android Central", "Android Central News", "http://www.androidcentral.com/rss.xml")
	}

	private FeedRequest createFeedRequest(
			String name = "Android Central",
			String description = "Android Central News",
			String url = "http://www.androidcentral.com/rss.xml") {

		return new FeedRequest(name, description, url)
	}
}
