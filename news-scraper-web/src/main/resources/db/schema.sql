--liquibase formatted sql

--changeset author:john id:initial
CREATE TABLE feeds (
	feed_id     UUID PRIMARY KEY,
	name        TEXT NOT NULL,
	description TEXT NOT NULL,
	url         TEXT NOT NULL UNIQUE,
	created     DATE NOT NULL,
	updated     DATE NOT NULL
);

CREATE TABLE articles (
	article_id       UUID PRIMARY KEY,
	feed_id          UUID NOT NULL REFERENCES feeds,
	guid             TEXT NOT NULL UNIQUE,
	title            TEXT,
	link             TEXT,
	description      TEXT,
	publication_date DATE,
	content          TEXT,
	raw_content      TEXT,
	created          DATE NOT NULL,
	updated          DATE NOT NULL
);

