package com.brainardphotography.scraper.config

import liquibase.integration.spring.SpringLiquibase
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import javax.sql.DataSource

@Configuration
open class DataConfig {
	@Bean
	open fun liquibase(dataSource: DataSource): SpringLiquibase {
		val liquibase = SpringLiquibase()
		liquibase.dataSource = dataSource
		liquibase.changeLog = "classpath:/db/changelog.xml"
		liquibase.defaultSchema = "scraper"

		return liquibase
	}
}