package com.brainardphotography.scraper.config

import com.brainardphotography.scraper.Scraper
import okhttp3.OkHttpClient
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
open class ScraperConfig {
	@Bean
	open fun scraper(httpClient: OkHttpClient): Scraper {
		return Scraper(httpClient)
	}
}