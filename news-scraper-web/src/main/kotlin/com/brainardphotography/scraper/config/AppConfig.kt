package com.brainardphotography.scraper.config

import com.brainardphotography.scraper.indexer.IndexerConfiguration
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.scheduling.annotation.EnableAsync
import org.springframework.scheduling.annotation.EnableScheduling

@EnableAsync
@EnableScheduling
@Configuration
open class AppConfig {
	@Bean
	open fun httpClient(indexerConfig: IndexerConfiguration): OkHttpClient {
		val interceptor = Interceptor { chain: Interceptor.Chain ->
			val request = chain.request()
			val newRequest = request.newBuilder()
					.addHeader("User-Agent", indexerConfig.userAgent)
					.build()

			chain.proceed(newRequest)
		}

		return OkHttpClient.Builder()
				.addInterceptor(interceptor)
				.build()
	}
}