package com.brainardphotography.scraper

import com.brainardphotography.scraper.indexer.IndexerConfiguration
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Bean
import org.springframework.scheduling.annotation.EnableAsync
import org.springframework.scheduling.annotation.EnableScheduling

@SpringBootApplication
open class WebServicesApplication {
}

fun main(args: Array<String>) {
	SpringApplication.run(WebServicesApplication::class.java, *args)
}
