package com.brainardphotography.scraper.indexer

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty
import java.util.*

class IndexerRequest {
	val feedId: UUID?

	companion object {
		const val JSON_CONTENT_TYPE = "application/vnd.indexer-command+json"
	}

	@JsonCreator
	constructor(@JsonProperty("feed_id") feedId: UUID?) {
		this.feedId = feedId
	}
}