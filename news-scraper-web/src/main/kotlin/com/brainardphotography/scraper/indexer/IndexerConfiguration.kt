package com.brainardphotography.scraper.indexer

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component

@ConfigurationProperties(prefix = "indexer")
@Component
open class IndexerConfiguration {

	/**
	 * User agent header to use when requesting feeds and articles
	 */
	lateinit var userAgent: String
}