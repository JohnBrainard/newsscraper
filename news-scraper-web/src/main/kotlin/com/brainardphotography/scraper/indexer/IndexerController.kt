package com.brainardphotography.scraper.indexer

import com.brainardphotography.scraper.indexer.service.IndexerService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/indexer")
class IndexerController {
	private val indexerService: IndexerService

	@Autowired
	constructor(indexerService: IndexerService) {
		this.indexerService = indexerService
	}

	@RequestMapping(
			method = arrayOf(RequestMethod.POST),
			consumes = arrayOf(IndexerRequest.JSON_CONTENT_TYPE))
	fun index(@RequestBody request: IndexerRequest): ResponseEntity<Any> {
		if (request.feedId != null) {
			indexerService.indexFeed(request.feedId)
		} else {
			indexerService.indexFeeds()
		}

		return ResponseEntity(HttpStatus.OK)
	}
}