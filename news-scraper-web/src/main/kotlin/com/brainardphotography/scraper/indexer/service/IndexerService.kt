package com.brainardphotography.scraper.indexer.service

import com.brainardphotography.scraper.FeedEntry
import com.brainardphotography.scraper.Scraper
import com.brainardphotography.scraper.articles.data.Article
import com.brainardphotography.scraper.articles.service.ArticleService
import com.brainardphotography.scraper.feeds.data.Feed
import com.brainardphotography.scraper.feeds.service.FeedService
import com.brainardphotography.scraper.indexer.IndexerException
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.scheduling.annotation.Async
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import java.net.URL
import java.util.*

@Service
open class IndexerService {
	private val logger by lazy {
		LoggerFactory.getLogger(IndexerService::class.java)
	}

	private val scraper: Scraper
	private val feedService: FeedService
	private val articleService: ArticleService

	@Autowired
	constructor(scraper: Scraper, feedService: FeedService, articleService: ArticleService) {
		this.scraper = scraper
		this.feedService = feedService
		this.articleService = articleService
	}

	@Async
	open fun indexFeed(feedId: UUID) {
		val feed = feedService.getFeed(feedId) ?: throw IndexerException("Feed $feedId not found")
		indexFeed(feed)
	}

	@Async
	open fun indexFeeds() {
		runIndexer()
	}

	@Scheduled(cron = "0 0 * * * *")
	open fun scheduledFeedIndex() {
		runIndexer()
	}

	private fun runIndexer() {
		val feeds = feedService.listFeeds()
		feeds.forEach {
			indexFeed(it)
		}
		logger.info("Indexing complete")
	}

	private fun indexFeed(feed: Feed) {
		if (feed.id == null) {
			throw IllegalArgumentException("Feed has no id.")
		}

		logger.info("Indexing feed: $feed")
		scraper.getArticleEntries(URL(feed.url)).forEach { entry: FeedEntry ->
			if (articleService.findArticleWithGuid(entry.id) == null) {
				indexArticle(feed, entry)
			}
		}
	}

	private fun indexArticle(feed: Feed, entry: FeedEntry) {
		logger.info("Indexing ${entry.title} : ${entry.url}")

		val rawContent = scraper.getArticleContent(entry.url)
		val article = Article(null,
				feed.id!!,
				entry.id,
				entry.title,
				entry.uri,
				entry.description,
				entry.contents,
				rawContent,
				entry.publicationDate)

		articleService.save(article)
	}
}
