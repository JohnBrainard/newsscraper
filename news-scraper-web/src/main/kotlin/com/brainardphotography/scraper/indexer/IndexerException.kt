package com.brainardphotography.scraper.indexer

class IndexerException : RuntimeException {
	constructor(message:String) : super(message)
	constructor(message:String, cause:Throwable) : super(message, cause)
}