package com.brainardphotography.scraper.feeds;

import com.brainardphotography.scraper.feeds.data.Feed
import com.fasterxml.jackson.annotation.JsonPropertyOrder
import java.util.*

@JsonPropertyOrder("id", "name", "description", "url")
data class FeedResponse(
		val id: UUID,
		val name: String,
		val description: String,
		val url: String) {

	companion object {
		const val JSON_CONTENT_TYPE = "application/vnd.feed+json"
	}
}

fun Feed.toFeedResponse() = FeedResponse(this.id!!, this.name, this.description, this.url)