package com.brainardphotography.scraper.feeds

data class FeedsResponse(val feeds: Collection<FeedResponse>) {
	companion object {
		const val JSON_CONTENT_TYPE = "application/vnd.feeds+json"
	}


}