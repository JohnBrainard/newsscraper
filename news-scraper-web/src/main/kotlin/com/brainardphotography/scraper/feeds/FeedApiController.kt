package com.brainardphotography.scraper.feeds

import com.brainardphotography.scraper.articles.ArticlesResponse
import com.brainardphotography.scraper.articles.service.ArticleService
import com.brainardphotography.scraper.articles.toArticleSummaryResponse
import com.brainardphotography.scraper.feeds.data.Feed
import com.brainardphotography.scraper.feeds.service.FeedService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@RequestMapping("/api/feeds")
class FeedApiController @Autowired constructor(feedService: FeedService, articleService: ArticleService) {
	private val feedService: FeedService
	private val articleService: ArticleService

	init {
		this.feedService = feedService
		this.articleService = articleService
	}

	@RequestMapping(
			method = arrayOf(RequestMethod.GET),
			produces = arrayOf(FeedsResponse.JSON_CONTENT_TYPE))
	fun feeds(): ResponseEntity<FeedsResponse> {
		val feeds = feedService.listFeeds().map { it.toFeedResponse() }

		return ResponseEntity(FeedsResponse(feeds), HttpStatus.OK)
	}

	@RequestMapping(
			value = "/{uuid}",
			method = arrayOf(RequestMethod.GET),
			produces = arrayOf(FeedResponse.JSON_CONTENT_TYPE))
	fun feed(@PathVariable("uuid") uuid: UUID): ResponseEntity<FeedResponse> {
		val feed = feedService.getFeed(uuid) ?: return ResponseEntity(HttpStatus.NOT_FOUND)

		return ResponseEntity(feed.toFeedResponse(), HttpStatus.OK)
	}

	@RequestMapping(
			value = "/{uuid}/articles",
			method = arrayOf(RequestMethod.GET),
			produces = arrayOf(ArticlesResponse.JSON_CONTENT_TYPE))
	fun feedArticles(@PathVariable("uuid") uuid: UUID): ResponseEntity<ArticlesResponse> {
		if (!feedService.exists(uuid)) {
			return ResponseEntity(HttpStatus.NOT_FOUND)
		}

		val articles = articleService.getArticles(uuid).map { it.toArticleSummaryResponse() }
		val response = ArticlesResponse(articles)
		return ResponseEntity(response, HttpStatus.OK)
	}

	@RequestMapping(
			method = arrayOf(RequestMethod.POST),
			consumes = arrayOf(FeedRequest.JSON_CONTENT_TYPE),
			produces = arrayOf(FeedResponse.JSON_CONTENT_TYPE))
	fun createFeed(@RequestBody feedRequest: FeedRequest): ResponseEntity<FeedResponse> {
		if (feedService.getFeedForUrl(feedRequest.url) != null) {
			return ResponseEntity(HttpStatus.CONFLICT)
		}

		val savedFeed = feedService.save(feedRequest.toFeed())

		return ResponseEntity(savedFeed.toFeedResponse(), HttpStatus.CREATED)
	}

	@RequestMapping(
			value = "/{uuid}",
			method = arrayOf(RequestMethod.PUT),
			consumes = arrayOf("application/vnd.feed+json"),
			produces = arrayOf("application/vnd.feed+json"))
	fun updateFeed(@PathVariable("uuid") uuid: UUID, @RequestBody feedRequest: FeedRequest): ResponseEntity<FeedResponse> {
		val feed: Feed = feedService.getFeed(uuid) ?: return ResponseEntity(HttpStatus.NOT_FOUND)

		feed.name = feedRequest.name
		feed.description = feedRequest.description

		val savedFeed = feedService.save(feed)

		return ResponseEntity(savedFeed.toFeedResponse(), HttpStatus.CREATED)
	}

	@RequestMapping(value = "/{uuid}", method = arrayOf(RequestMethod.DELETE))
	fun deleteFeed(@PathVariable("uuid") uuid: UUID): ResponseEntity<Any?> {
		feedService.delete(uuid)
		return ResponseEntity(HttpStatus.OK)
	}
}