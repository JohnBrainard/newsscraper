package com.brainardphotography.scraper.feeds.service

import com.brainardphotography.scraper.feeds.data.Feed
import com.brainardphotography.scraper.feeds.data.FeedRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*

@Service
class DefaultFeedService : FeedService {
	private val feedRepository: FeedRepository

	@Autowired
	constructor(feedRepository: FeedRepository) {
		this.feedRepository = feedRepository
	}

	override fun getFeed(id: UUID): Feed? = feedRepository.findOne(id)

	override fun getFeedForUrl(url: String): Feed? = feedRepository.findByUrl(url)

	override fun listFeeds() = feedRepository.findAll()

	override fun exists(uuid: UUID) = feedRepository.exists(uuid)

	override fun save(feed: Feed): Feed = feedRepository.save(feed)

	override fun delete(uuid: UUID) = feedRepository.delete(uuid)

	override fun delete(feed: Feed) = feedRepository.delete(feed)

}