package com.brainardphotography.scraper.feeds.service

import com.brainardphotography.scraper.feeds.data.Feed
import java.util.*

interface FeedService {
	fun getFeed(id: UUID): Feed?

	fun getFeedForUrl(url: String): Feed?

	fun listFeeds(): Iterable<Feed>

	fun exists(uuid:UUID):Boolean

	fun save(feed: Feed): Feed

	fun delete(uuid: UUID)

	fun delete(feed: Feed)

}