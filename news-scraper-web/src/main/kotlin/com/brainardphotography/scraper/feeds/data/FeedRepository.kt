package com.brainardphotography.scraper.feeds.data

import org.springframework.data.repository.CrudRepository
import java.util.*

interface FeedRepository : CrudRepository<Feed, UUID> {
	fun findByUrl(url: String): Feed
}