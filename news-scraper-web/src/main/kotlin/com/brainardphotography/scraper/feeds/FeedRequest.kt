package com.brainardphotography.scraper.feeds

import com.brainardphotography.scraper.feeds.data.Feed
import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonPropertyOrder
import java.util.*

@JsonPropertyOrder("name", "desription", "url")
class FeedRequest {
	val name: String
	val description: String
	val url: String

	companion object {
		const val JSON_CONTENT_TYPE = "application/vnd.feed+json"
	}

	@JsonCreator
	constructor(
			@JsonProperty("name") name: String,
			@JsonProperty("description") description: String,
			@JsonProperty("url") url: String
	) {
		this.name = name
		this.description = description
		this.url = url
	}
}

fun FeedRequest.toFeed(id: UUID? = null) = Feed(id, this.name, this.description, this.url)
