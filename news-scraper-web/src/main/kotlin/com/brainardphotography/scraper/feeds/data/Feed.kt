package com.brainardphotography.scraper.feeds.data

import org.hibernate.annotations.GenericGenerator
import org.hibernate.annotations.Type
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "feeds")
class Feed() {

	constructor(name: String, description: String, url: String) : this(null, name, description, url)

	constructor(id: UUID?, name: String, description: String, url: String) : this() {
		if (id != null)
			this.id = id

		this.name = name
		this.description = description
		this.url = url
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid2")
	@Column(name = "feed_id")
	@Type(type = "pg-uuid")
	var id: UUID? = null

	@Column
	lateinit var name: String

	@Column
	lateinit var description: String

	@Column
	lateinit var url: String

	@Column
	lateinit var created: Date

	@Column
	lateinit var updated: Date

	@PrePersist
	fun prePersist() {
		created = Date()
		updated = created
	}

	@PreUpdate
	fun preUpdate() {
		updated = Date()
	}

	override fun toString() = "Feed(id:$id, name:$name, url:$url)"
}