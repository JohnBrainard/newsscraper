package com.brainardphotography.scraper.web

import com.brainardphotography.scraper.articles.data.Article
import com.brainardphotography.scraper.articles.service.ArticleService
import com.brainardphotography.scraper.feeds.data.Feed
import com.brainardphotography.scraper.feeds.service.FeedService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod

data class FeedAndArticles(val feed: Feed, val articles: Iterable<Article>)

@Controller
@RequestMapping("/")
class HomeController {

	private val feedService: FeedService
	private val articleService: ArticleService

	@Autowired
	constructor(feedService: FeedService, articleService: ArticleService) {
		this.feedService = feedService
		this.articleService = articleService
	}

	@RequestMapping(method = arrayOf(RequestMethod.GET))
	fun index(model: Model): String {
		val feeds = feedService.listFeeds()

		model.addAttribute("feeds", feeds.map {
			FeedAndArticles(it, articleService.getArticles(it.id!!, 3, 0))
		})

		return "index"
	}
}