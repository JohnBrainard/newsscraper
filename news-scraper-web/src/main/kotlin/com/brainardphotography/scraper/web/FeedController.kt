package com.brainardphotography.scraper.web

import com.brainardphotography.scraper.articles.service.ArticleService
import com.brainardphotography.scraper.feeds.service.FeedService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import java.util.*

@RequestMapping("/feeds")
@Controller
class FeedController {
	private val feedService: FeedService
	private val articleService: ArticleService

	@Autowired
	constructor(feedService: FeedService, articleService: ArticleService) {
		this.feedService = feedService
		this.articleService = articleService
	}

	@RequestMapping
	fun feeds(model: Model): String {
		val feeds = feedService.listFeeds()

		model.addAttribute("feeds", feeds)

		return "feeds/index"
	}

	@RequestMapping(value = "/{uuid}")
	fun feed(
			@PathVariable uuid: UUID,
			@RequestParam(value = "page", defaultValue = "0") page: Int,
			@RequestParam(value = "count", defaultValue = "25") pageSize: Int,
			model: Model): String {

		val feed = feedService.getFeed(uuid)
		val articles = articleService.getArticles(uuid, Math.min(pageSize, 25), page)

		model.addAttribute("feed", feed)
		model.addAttribute("articles", articles)

		return "feeds/feed"
	}
}