package com.brainardphotography.scraper.web

import com.brainardphotography.scraper.articles.service.ArticleService
import org.jsoup.Jsoup
import org.jsoup.safety.Whitelist
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import java.util.*

@Controller
@RequestMapping("/articles")
class ArticleController {
	private val articleService: ArticleService

	@Autowired
	constructor(articleService: ArticleService) {
		this.articleService = articleService
	}

	@RequestMapping("/{uuid}")
	fun article(@PathVariable("uuid") articleId: UUID, model: Model): String {
		val article = articleService.getArticle(articleId)

		model.addAttribute("article", article)
		model.addAttribute("articleHtml", scrapeContent(article!!.rawContent))

		return "articles/article"
	}

	private fun scrapeContent(content:String):String {
		val document = Jsoup.parse(content)
		val elements = document.select("div.the-content").first()

		return Jsoup.clean(elements.toString(), Whitelist.basicWithImages())
	}
}