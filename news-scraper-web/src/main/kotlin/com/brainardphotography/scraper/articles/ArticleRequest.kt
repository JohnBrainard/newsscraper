package com.brainardphotography.scraper.articles

import com.brainardphotography.scraper.articles.data.Article
import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty
import java.util.*

class ArticleRequest {
	val guid: String
	val feedId: UUID
	val title: String
	val link: String
	val description: String
	val content: String
	val rawContent: String
	val publicationDate: Date

	companion object {
		const val JSON_CONTENT_TYPE = ArticleSummaryResponse.JSON_CONTENT_TYPE
	}

	@JsonCreator
	constructor(
			@JsonProperty("guid") guid: String,
			@JsonProperty("feed_id") feedId: UUID,
			@JsonProperty("title") title: String,
			@JsonProperty("link") link: String,
			@JsonProperty("description") description: String,
			@JsonProperty("content") content: String,
			@JsonProperty("raw_content") rawContent: String,
			@JsonProperty("publication_date") publicationDate: Date) {

		this.guid = guid
		this.feedId = feedId
		this.title = title
		this.link = link
		this.description = description
		this.content = content
		this.rawContent = rawContent
		this.publicationDate = publicationDate
	}
}

fun ArticleRequest.toArticle() =
		Article(null, feedId, guid, title, link, description, content, rawContent, publicationDate)
