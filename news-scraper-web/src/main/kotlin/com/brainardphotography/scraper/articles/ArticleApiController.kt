package com.brainardphotography.scraper.articles

import com.brainardphotography.scraper.articles.service.ArticleService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.dao.DataAccessException
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.sql.SQLException
import java.util.*

@RestController
@RequestMapping("/api/articles")
class ArticleApiController {
	private val articleService: ArticleService
	private val logger: Logger by lazy {
		LoggerFactory.getLogger(ArticleApiController::class.java)
	}

	@Autowired
	constructor(articleService: ArticleService) {
		this.articleService = articleService
	}

	@RequestMapping(
			method = arrayOf(RequestMethod.GET),
			produces = arrayOf(ArticlesResponse.JSON_CONTENT_TYPE))
	fun articles(): ResponseEntity<ArticlesResponse> {
		val articles = articleService.listArticles().map { it.toArticleSummaryResponse() }
		return ResponseEntity(ArticlesResponse(articles), HttpStatus.OK)
	}

	@RequestMapping(
			value = "/{uuid}",
			method = arrayOf(RequestMethod.GET),
			produces = arrayOf(ArticleSummaryResponse.JSON_CONTENT_TYPE))
	fun article(@PathVariable("uuid") uuid: UUID): ResponseEntity<ArticleResponse> {
		val article = articleService.getArticle(uuid) ?: return ResponseEntity(HttpStatus.NOT_FOUND)
		return ResponseEntity(article.toArticleResponse(), HttpStatus.OK)
	}

	@RequestMapping(
			method = arrayOf(RequestMethod.POST),
			produces = arrayOf(ArticleSummaryResponse.JSON_CONTENT_TYPE),
			consumes = arrayOf(ArticleRequest.JSON_CONTENT_TYPE))
	fun create(@RequestBody articleRequest: ArticleRequest): ResponseEntity<ArticleSummaryResponse> {
		if (articleService.findArticleWithGuid(articleRequest.guid) != null) {
			return ResponseEntity(HttpStatus.CONFLICT)
		}

		val article = try {
			articleService.save(articleRequest.toArticle())
		} catch (ex: DataAccessException) {
			logger.error("Error saving article: ${articleRequest}", ex)
			return ResponseEntity(HttpStatus.BAD_REQUEST)
		}

		return ResponseEntity(article.toArticleSummaryResponse(), HttpStatus.OK)
	}
}