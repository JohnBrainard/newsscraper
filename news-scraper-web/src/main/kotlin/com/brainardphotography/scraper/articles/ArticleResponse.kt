package com.brainardphotography.scraper.articles

import com.brainardphotography.scraper.articles.data.Article
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonPropertyOrder
import java.util.*

@JsonPropertyOrder("id", "feed_id", "guid", "title", "link", "description", "content", "raw_content", "publication_date")
data class ArticleResponse(
		val id: UUID?,

		@JsonProperty("feed_id")
		val feedId: UUID,
		val guid: String,
		val title: String,
		val link: String,
		val description: String,
		val content: String,

		@JsonProperty("raw_content")
		val rawContent: String,

		@JsonProperty("publication_date")
		val publicationDate: Date) {

	companion object {
		const val JSON_CONTENT_TYPE = "application/vnd.article+json"
	}
}

fun Article.toArticleResponse() = ArticleResponse(
		id, feedId, guid, title, link, description, content, rawContent, publicationDate)

