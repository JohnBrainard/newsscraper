package com.brainardphotography.scraper.articles.service

import com.brainardphotography.scraper.articles.data.Article
import com.brainardphotography.scraper.articles.data.ArticleRepository
import com.brainardphotography.scraper.feeds.data.Feed
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import java.util.*

@Service
class DefaultArticleService : ArticleService {
	private val articleRepository: ArticleRepository

	@Autowired
	constructor(articleRepository: ArticleRepository) {
		this.articleRepository = articleRepository
	}

	override fun getArticle(id: UUID) = articleRepository.findOne(id)

	override fun getArticles(feedId:UUID) = articleRepository.findByFeedId(feedId)

	override fun getArticles(feedId: UUID, pageSize: Int, page: Int) = articleRepository.findByFeedId(feedId, PageRequest(page, pageSize))

	override fun findArticleWithGuid(guid: String) = articleRepository.findByGuid(guid)

	override fun listArticles() = articleRepository.findAll()

	override fun save(article: Article) = articleRepository.save(article)
}