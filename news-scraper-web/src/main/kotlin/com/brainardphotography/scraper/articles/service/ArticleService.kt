package com.brainardphotography.scraper.articles.service

import com.brainardphotography.scraper.articles.data.Article
import java.util.*

interface ArticleService {
	fun getArticle(id: UUID): Article?

	fun getArticles(feedId: UUID): Iterable<Article>

	fun getArticles(feedId: UUID, pageSize:Int, page:Int) : Iterable<Article>

	fun findArticleWithGuid(guid: String): Article?

	fun listArticles(): Iterable<Article>

	fun save(article: Article): Article
}