package com.brainardphotography.scraper.articles

import com.fasterxml.jackson.annotation.JsonPropertyOrder

@JsonPropertyOrder("count", "articles")
data class ArticlesResponse(val articles: Iterable<ArticleSummaryResponse>) {
	companion object {
		const val JSON_CONTENT_TYPE = "application/vnd.articles+json"
	}

	val count:Int get() = articles.count()
}