package com.brainardphotography.scraper.articles.data

import org.hibernate.annotations.GenericGenerator
import org.hibernate.annotations.Type
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "articles")
class Article() {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid2")
	@Column(name = "article_id")
	@Type(type = "pg-uuid")
	var id: UUID? = null

	@Column(name = "feed_id")
	@Type(type = "pg-uuid")
	lateinit var feedId: UUID

	@Column
	lateinit var guid: String

	@Column
	lateinit var title: String

	@Column
	lateinit var link: String

	@Column
	lateinit var description: String

	@Column
	lateinit var content: String

	@Column(name = "raw_content")
	lateinit var rawContent: String

	@Column(name = "publication_date")
	lateinit var publicationDate: Date

	@Column
	lateinit var created: Date

	@Column
	lateinit var updated: Date

	constructor(id: UUID?, feedId: UUID, guid: String, title: String, link: String, description: String, content: String, rawContent: String, publicationDate: Date) : this() {
		this.id = id
		this.feedId = feedId
		this.guid = guid
		this.title = title
		this.link = link
		this.description = description
		this.content = content
		this.rawContent = rawContent
		this.publicationDate = publicationDate
	}

	@PrePersist
	fun prePersist() {
		created = Date()
		updated = created
	}

	@PreUpdate
	fun preUpdate() {
		updated = Date()
	}
}