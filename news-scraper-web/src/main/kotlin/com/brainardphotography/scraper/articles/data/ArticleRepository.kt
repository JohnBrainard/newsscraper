package com.brainardphotography.scraper.articles.data

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.PagingAndSortingRepository
import java.util.*

interface ArticleRepository : PagingAndSortingRepository<Article, UUID> {
	fun findByGuid(guid: String): Article

	@Query("select a from Article a where a.feedId=? order by publicationDate desc")
	fun findByFeedId(feedId: UUID): Iterable<Article>

	@Query("select a from Article a where a.feedId=? order by publicationDate desc")
	fun findByFeedId(feedId: UUID, pageable: Pageable): Page<Article>
}