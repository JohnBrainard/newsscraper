#!/usr/bin/env bash

PGDATA=$HOME/Documents/DockerData/scraper

docker run --name pgdb-scraper -d \
		-p 5432:5432 \
		-e POSTGRES_USER=postgres \
        -e POSTGRES_PASSWORD=password \
        -e PGDATA=/var/lib/postgresql/data/pgdata \
        -v $PGDATA:/var/lib/postgresql/data/pgdata \
        postgres:latest