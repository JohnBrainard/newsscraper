package com.brainardphotography.scraper

import org.jsoup.nodes.Element
import java.net.URL

class Feed(val url: URL, val type: String = "application/rss+xml") {

	override fun toString(): String = "Feed(href=$url, type=$type)"

	companion object {
		val RSS_CONTENT_TYPE = "application/rss+xml"

		internal fun fromDocument(element: Element, url: URL? = null): Feed {
			val feedUrl = if (url != null) {
				URL(url, element.attr("href"))
			} else {
				URL(element.attr("href"))
			}

			return Feed(feedUrl, element.attr("type"))
		}
	}
}