package com.brainardphotography.scraper

import com.rometools.rome.feed.synd.SyndEntry
import java.net.URL
import java.util.*

data class FeedEntry(
		val uri: String,
		val id: String,
		val url: URL,
		val title: String,
		val description: String,
		val contents: String,
		val publicationDate:Date) {

	internal companion object {
		fun create(entry: SyndEntry): FeedEntry {
			val url = URL(entry.link)
			val title = entry.title
			val description = entry.description.value
			val contents = entry.contents.map { it.value }.joinToString(" ")
			val id = Base64.getUrlEncoder().encodeToString(entry.uri.toByteArray())
			val date = entry.publishedDate

			return FeedEntry(entry.uri, id, url, title, description, contents, date)
		}
	}
}
