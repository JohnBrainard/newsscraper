package com.brainardphotography.scraper.repo

class NewsArticle {
	val guid: String
	val title: String
	val description: String

	internal constructor(builder: Builder) {
		this.guid = builder.guid ?: throw IllegalArgumentException("guid must be specified")
		this.title = builder.title ?: throw IllegalArgumentException("title must be specified")
		this.description = builder.description ?: throw IllegalArgumentException("description must be specified")
	}

	class Builder {
		var guid: String? = null
		var title: String? = null
		var description: String? = null
		var content: String? = null

		fun guid(value: String): Builder {
			guid = value
			return this
		}

		fun title(value: String): Builder {
			title = value
			return this
		}

		fun description(value: String): Builder {
			description = value
			return this
		}

		fun content(value: String): Builder {
			content = value
			return this
		}

		fun build(): NewsArticle = NewsArticle(this)
	}
}