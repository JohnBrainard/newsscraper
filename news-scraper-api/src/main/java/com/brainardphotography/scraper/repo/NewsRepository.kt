package com.brainardphotography.scraper.repo

import org.apache.solr.client.solrj.SolrQuery
import org.apache.solr.client.solrj.impl.HttpSolrClient
import java.util.*

class NewsRepository(val host:String="http://localhost:8983/solr/news_index") {

	fun listArticles():List<String> {
		val solr = HttpSolrClient.Builder(host).build()

		val query = SolrQuery("*:*")
		val response = solr.query(query)

		return response.results.map {
			it.toString()
		}
	}
}

fun main(vararg args:String) {
	val repo = NewsRepository()
	repo.listArticles()

	val guid = Base64.getUrlEncoder().encodeToString("55336 at http://www.androidcentral.com".toByteArray())

	println("GUID: ${guid}")
}