package com.brainardphotography.scraper

import com.rometools.rome.io.SyndFeedInput
import com.rometools.rome.io.XmlReader
import okhttp3.OkHttpClient
import okhttp3.Request
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import java.net.URL

class Scraper {
	private val http: OkHttpClient

	constructor(httpClient: OkHttpClient) {
		this.http = httpClient
	}

	fun findRssFeeds(url: URL): Collection<Feed> {
		val doc = loadDocument(url)
		return doc.select("link[type=${Feed.RSS_CONTENT_TYPE}]").map {
			Feed.fromDocument(it, url)
		}
	}

	fun getArticleEntries(feedUrl: URL): Collection<FeedEntry> {
		val request = Request.Builder().url(feedUrl).build()
		val response = http.newCall(request).execute()

		val input = SyndFeedInput()
		val feed = input.build(XmlReader(response.body().byteStream()))

		return feed.entries.map {
			FeedEntry.create(it)
		}
	}

	fun getArticleEntries(feed: Feed): Collection<FeedEntry> = getArticleEntries(feed.url)

	fun getArticleContent(url: URL): String {
		val request = Request.Builder()
				.url(url)
				.build()
		val response = http.newCall(request).execute()
		return response.body().string()
	}

	fun findRssFeeds(url: String): Collection<Feed> = findRssFeeds(URL(url))

	fun getUrl(url:URL):URL {
		val request = Request.Builder()
				.head()
				.build()

		val response = http.newCall(request).execute()
		if (response.code() == 302) {
			return URL(response.header("Location"))
		}

		return url
	}

	private fun loadDocument(url: URL): Document {
		val request = Request.Builder()
				.url(url)
				.build()
		val response = http.newCall(request).execute()
		return Jsoup.parse(response.body().string())
	}
}

fun main(vararg args: String) {
	val scraper = Scraper(OkHttpClient())

	val feeds = scraper.findRssFeeds("http://www.androidcentral.com/")
	feeds.forEach {
		print("Feed: ${it}")
		scraper.getArticleEntries(it).forEach { println(it) }
	}
}